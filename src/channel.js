const { client } = require('./discordClient')
const { musicChannels } = require('./musicChannels')

exports.channelsIn = new Map()

const channelsIn = exports.channelsIn

client.on('voiceStateUpdate', (oldMember, newMember) => {
  if (newMember.voiceChannel !== oldMember.voiceChannel) {
    if (newMember.voiceChannel != null) {
      let voiceChannel = newMember.voiceChannel
      // If I joined the channel claim it
      if (newMember.id == client.user.id) {
        channelsIn.set(newMember.guild.id)
      }
      // If someone joined my channel
      if (
        voiceChannel.members.size == 2 &&
        newMember.guild.me &&
        voiceChannel == newMember.guild.me.voiceChannel
      ) {
        if (voiceChannel.connection && voiceChannel.connection.dispatcher) {
          try {
            setTimeout(() => {
              console.log('resume')
              voiceChannel.connection.dispatcher.resume()
            }, 1000)
          } catch (e) {
            console.log(e)
          }
        }
      }
    }
    if (oldMember.voiceChannel != null) {
      // If I left the channel make myself available
      if (
        oldMember.id == client.user.id &&
        channelsIn.has(oldMember.guild.id)
      ) {
        if (newMember.voiceChannel) {
          channelsIn.set(newMember.guild.id, newMember.voiceChannel.id)
        } else {
          channelsIn.delete(oldMember.guild.id)
        }
      }
      // If I'm the only one left. I leave
      if (
        oldMember.voiceChannel.members.size === 1 &&
        oldMember.guild.me &&
        oldMember.voiceChannel == oldMember.guild.me.voiceChannel
      ) {
        // Try to leave channel
        try {
          // Dissolve or pause dispatcher
          if (
            oldMember.voiceChannel.connection &&
            oldMember.voiceChannel.connection.dispatcher
          ) {
            musicChannel = musicChannels.get(oldMember.voiceChannel.id)
            if (musicChannel && musicChannel.stayInVoice) {
              // Pause the music
              console.log('pause')
              oldMember.voiceChannel.connection.dispatcher.pause()
            } else {
              // Destroy queue
              if (musicChannel) musicChannel.queue = []
              try {
                oldMember.voiceChannel.connection.dispatcher.end()
              } catch (e) {}
              // Leave
              try {
                oldMember.voiceChannel.leave()
              } catch (e) {}
              channelsIn.delete(oldMember.guild.id)
            }
          }
        } catch (err) {
          console.log(err)
        }
      }
    }
  }
})
